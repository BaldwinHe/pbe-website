# Picture Book Evaluation Website

## Requirement
> 小何 Local Environment
* node v10.16.3
* npm v6.9.0

## Build Setup
```bash
# clone and enter the repo
git clone https://gitlab.com/BaldwinHe/pbe-website.git
cd pbe-website
```

### Server
```bash
# enter server dir
cd server

# install dependencies
npm install
npm audit fix

# start server
node app.js
```

### Client
```bash
# create a new terminal
# enter client dir
cd path/to/pbe-website

# install dependencies
npm install
npm audit fix

# serve with hot reload at localhost:8080
npm run dev

# then open localhost:8080
```

## Note
* `src/views/RAW` 为参考Repo源码
